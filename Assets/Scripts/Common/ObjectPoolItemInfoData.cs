﻿using System.Collections;
using UnityEngine;


public class ObjectPoolItemInfoData : MonoBehaviour, IGetObjectPoolItem
{
    public ObjectPoolItem objectPoolItem;

    public ObjectPoolItem GetObjectPoolItem()
    {
        return objectPoolItem;
    }
}