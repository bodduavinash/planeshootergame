﻿using System.Collections;
using UnityEngine;

public static class Utils
{
    public static Rect GetSpriteRect(Transform transform)
    {
        return transform.GetComponent<SpriteRenderer>().sprite.rect;
    }

    public static Vector2 GetSpriteSize(Transform transform)
    {
        return transform.GetComponent<SpriteRenderer>().size;
    }
}