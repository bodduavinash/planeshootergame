﻿using System.Collections;
using UnityEngine;

public class ObjectPoolDataTag : MonoBehaviour, IGetObjectPoolTag
{
    [SerializeField]
    private string poolNameTag = "";

    public string GetObjectPoolTag()
    {
        return poolNameTag;
    }
}