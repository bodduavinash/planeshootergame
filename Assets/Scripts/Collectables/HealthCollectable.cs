using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthCollectable : MonoBehaviour, ICollectables
{
    public void Collect(GameObject gameObject)
    {
        GameController.Instance.GetPlayerController.GetComponent<IAddHealth>().AddHealth(GameController.Instance.CollectablesController.CollectableData.health);
        Destroy(gameObject);
    }
}
