using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootCollectable : MonoBehaviour, ICollectables
{
    public void Collect(GameObject gameObject)
    {
        GameController.Instance.GetPlayerController.ChangeShootingType();
        Destroy(gameObject);
    }
}
