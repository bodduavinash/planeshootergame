using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "PlayerDataProfile", menuName = "Avinash/PlayerSO", order = 1)]
public class PlayerScriptableObject : ScriptableObject
{
    [Header("Player Movement Configuration")]
    public Vector3 startPosition;
    public float moveToDefaultDuration;
    public float movementSpeed;
    public DG.Tweening.Ease easeType;

    [Header("Player Health")]
    public float maxHealth;

    [Header("Bullet Configuration")]
    public GameObject bulletPrefab;
    public int numberOfBullets;
    public float bulletSpeed;
    public float bulletRateOfFire;
    public float bulletDamage;
}
