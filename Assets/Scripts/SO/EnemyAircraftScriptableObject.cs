using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "EnemyAircraftData", menuName = "Avinash/EnemyAircraftSO", order = 2)]
public class EnemyAircraftScriptableObject : ScriptableObject
{
    public Sprite enemySprite;
    public float maxHealth;
    public int points;

    [Header("Enemy Movement configuration")]
    public Vector3 stopPosition;
    public float movementSpeed;
    //public Vector3 clampPosition;
    public DG.Tweening.Ease easeType; 

    [Header("Enemy Bullet Configuration")]
    public GameObject enemyBulletPrefab;
    public float bulletDamage;
    public float bulletSpeed;
    public float rateOfFire;
}
