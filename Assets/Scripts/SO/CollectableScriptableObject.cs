﻿using UnityEngine;

[CreateAssetMenu(fileName = "CollectableData", menuName ="Avinash/CollectablesSO", order = 4)]
public class CollectableScriptableObject : ScriptableObject
{
    [Header("Health")]
    public int health;
}