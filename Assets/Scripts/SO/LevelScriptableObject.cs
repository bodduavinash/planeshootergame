﻿using UnityEngine;

[CreateAssetMenu(fileName = "LevelData", menuName = "Avinash/LevelSO", order = 3)]
public class LevelScriptableObject : ScriptableObject
{
    public PlaneType PlanesType1;
    public PlaneType PlanesType2;
    public PlaneType PlanesType3;
}

[System.Serializable]
public class PlaneType
{
    public int noOfPlanesOfType;
    public int minPlayerScoreToSpawn;
}