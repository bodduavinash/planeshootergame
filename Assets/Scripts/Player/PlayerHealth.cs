
using UnityEngine;

public class PlayerHealth : MonoBehaviour, IApplyDamage, IAddHealth
{
    PlayerScriptableObject playerData;

    private float health;

    private const string healthProperty = "_Health";

    public void Start()
    {
        playerData = GetComponent<PlayerController>().playerData;

        health = playerData.maxHealth;
    }

    public void ApplyDamage(float damage)
    {
        health -= damage;

        gameObject.GetComponentInChildren<MeshRenderer>()?.material.SetFloat(healthProperty, health / playerData.maxHealth);

        if(health <= 0)
        {
            GameController.Instance.IsGameOver = true;
            GameController.Instance.IsGameStarted = false;
            GameController.Instance.GetUIController.ShowGameOverPanel(true);
        }
    }

    public void AddHealth(float health)
    {
        this.health += health;

        if(this.health > playerData.maxHealth)
        {
            this.health = playerData.maxHealth;
        }

        gameObject.GetComponentInChildren<MeshRenderer>()?.material.SetFloat(healthProperty, this.health / playerData.maxHealth);
    }
}
