using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerShoot : IPooledObject, IShootable
{
    private readonly PlayerScriptableObject playerData;
    private Transform playerTransform;

    private GameObjectPool bulletPool;

    private float currentTime;

    public GameObjectPool GetPooledObject { get => bulletPool;}

    public PlayerShoot(PlayerScriptableObject playerData, Transform playerTransform)
    {
        this.playerData = playerData;
        this.playerTransform = playerTransform;

        bulletPool = new GameObjectPool("BulletPool");
        bulletPool.InitGameObjectsPool(playerData.bulletPrefab, playerData.numberOfBullets);
    }

    public void Update()
    {
        if (!GameController.Instance.IsGameStarted)
            return;

        Shoot();
    }

    public void Shoot()
    {
        currentTime += Time.deltaTime;

        if (currentTime > playerData.bulletRateOfFire)
        {
            currentTime = 0f;

            var bullet = bulletPool.GetObjectFromPool();
            bullet.transform.position = playerTransform.position;
            bullet.transform.rotation = Quaternion.identity;
            bullet.GetComponent<Rigidbody2D>().AddForce(bullet.transform.up * playerData.bulletSpeed, ForceMode2D.Impulse);
        }
    }
}