﻿using System.Collections;
using UnityEngine;


public class PlayerShootCollectible : IPooledObject, IShootable
{
    private PlayerScriptableObject playerData;
    private Transform playerTransform;

    private GameObjectPool bulletPool;

    private float currentTime;

    private float stepPosition = 0.15f;
    private float stepRotation = 10f;

    private float startPosition = -0.24f;
    private float startRotation = -20f;

    public GameObjectPool GetPooledObject { get => bulletPool; }

    public PlayerShootCollectible(PlayerScriptableObject playerData, Transform playerTransform)
    {
        this.playerData = playerData;
        this.playerTransform = playerTransform;

        bulletPool = new GameObjectPool("BulletPool");
        bulletPool.InitGameObjectsPool(playerData.bulletPrefab, playerData.numberOfBullets);
    }

    public void Shoot()
    {
        currentTime += Time.deltaTime;

        if (currentTime > playerData.bulletRateOfFire)
        {
            currentTime = 0f;

            for(int i = 0; i < 4; i++)
            {
                var bullet = GetPooledObject.GetObjectFromPool();
                bullet.transform.position = playerTransform.position + (Vector3.right * startPosition) + (Vector3.right * (stepPosition * i));
                bullet.transform.rotation = Quaternion.Euler((Vector3.forward * startRotation) + (Vector3.forward * (stepRotation * i)));
                bullet.GetComponent<Rigidbody2D>().AddForce(bullet.transform.up * playerData.bulletSpeed, ForceMode2D.Impulse);
            }
        }
    }
}