using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerScore
{
    private int score;

    private int highScore;

    public int GetScore => score;

    public int HighScore { get => highScore; set => highScore = value; }

    public PlayerScore()
    {
        score = 0;
    }

    public void AddScore(TMPro.TextMeshPro text, int score)
    {
        this.score += score;

        text.text = this.score.ToString();
    }
}
