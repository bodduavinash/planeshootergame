using UnityEngine;
using DG.Tweening;

public class PlayerMovement : IPlayerTransform
{
    private PlayerInputActions inputActions;
    private PlayerScriptableObject playerData;
    private Rigidbody2D playerRigidbody;
    
    private Vector3 moveVector;

    private readonly Rect spriteSize;

    private bool canUseTouch = true;
    
    public Transform PlayerTransform => playerRigidbody.transform;

    public PlayerMovement(PlayerInputActions input, PlayerScriptableObject playerData, Rigidbody2D rigidbody)
    {
        inputActions = input;
        this.playerData = playerData;
        playerRigidbody = rigidbody;

        spriteSize = Utils.GetSpriteRect(rigidbody.transform);
        
        MoveToDefaultPos();
    }

    public void Update()
    {
        if (!canUseTouch)
            return;

        Move();
    }

    private void Move()
    {
        moveVector = inputActions.Touch.PrimaryPosition.ReadValue<Vector2>();

        moveVector = CheckBoundary(moveVector);
        moveVector.z = Camera.main.nearClipPlane;

        moveVector = Camera.main.ScreenToWorldPoint(moveVector);
        
        if(inputActions.Touch.PrimaryTouch.ReadValue<float>() > 0)
        {
            //playerRigidbody.transform.DOMove(moveVector, playerData.movementSpeed).SetAutoKill(false);
            playerRigidbody.transform.position = Vector3.Lerp(playerRigidbody.transform.position, moveVector,playerData.movementSpeed * Time.deltaTime);
        }
    }

    private Vector3 CheckBoundary(Vector2 vector)
    {

        vector.x = Mathf.Clamp(vector.x, 0 + (spriteSize.width / 2) - spriteSize.x, Screen.width - (spriteSize.width )/ 2 + spriteSize.x);
        vector.y = Mathf.Clamp(vector.y, 0 + spriteSize.height / 2, Screen.height - spriteSize.height / 2);

        return vector;
    }

    private void MoveToDefaultPos()
    {
        playerRigidbody.transform.DOMove(playerData.startPosition, playerData.moveToDefaultDuration).SetAutoKill(true).SetEase(playerData.easeType)
            .OnComplete(() => { canUseTouch = true; });
    }
}