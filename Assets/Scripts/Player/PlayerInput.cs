using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerInput
{
    private PlayerInputActions playerInputAction;

    public PlayerInputActions PlayerInputAction { get => playerInputAction; }

    public PlayerInput()
    {
        Init();
    }

    ~PlayerInput()
    {
        Delete();
    }

    private void Init()
    {
        playerInputAction = new PlayerInputActions();
        playerInputAction.Enable();
    }

    private void Delete()
    {
        playerInputAction.Disable();
        playerInputAction.Dispose();
        playerInputAction = null;
    }
}
