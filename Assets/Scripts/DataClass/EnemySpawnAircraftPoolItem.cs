﻿[System.Serializable]
public class EnemySpawnAircraftPoolItem
{
    public string poolNameTag;
    public ObjectPoolItem enemySpawnAircraftPool;
    public ObjectPoolItem enemyBulletPool;
    public EnemySpawnAircraftPoolItem GetEnemySpawnPooledObjectItem => this;
}