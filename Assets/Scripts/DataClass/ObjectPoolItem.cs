﻿
[System.Serializable]
public class ObjectPoolItem
{
    public UnityEngine.GameObject prefabGameObject;
    public int noOfObjectsToPool;

    private GameObjectPool gameObjectPool;

    public GameObjectPool GetPooledObjectItem => gameObjectPool;

    public ObjectPoolItem(string poolNameTag, UnityEngine.GameObject prefab, int noOfObjectsToPool)
    {
        this.prefabGameObject = prefab;
        this.noOfObjectsToPool = noOfObjectsToPool;

        gameObjectPool = new GameObjectPool(poolNameTag);
        gameObjectPool.InitGameObjectsPool(prefab, noOfObjectsToPool);
    }
}