﻿using System.Collections;
using UnityEngine;

public class EnemyMovement : MonoBehaviour, IEnemyMove
{
    protected Vector2 cameraToWorld;

    private void Awake()
    {
        cameraToWorld = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, Camera.main.nearClipPlane));
    }

    protected virtual void OnEnable(){}

    protected virtual void OnDisable(){}

    public virtual void EnemyMove(){}
}