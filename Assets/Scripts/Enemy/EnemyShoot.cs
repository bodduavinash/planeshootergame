﻿using System.Collections;
using UnityEngine;

public class EnemyShoot : MonoBehaviour, IShootable
{
    private EnemyAircraftScriptableObject enemyAircraftData;

    private float currentTime;

    private void OnEnable()
    {
        enemyAircraftData = GetComponent<EnemyAI>().enemyAircraftData;
        currentTime = 0.0f;
    }

    public void Shoot()
    {
        currentTime += Time.deltaTime;

        if (currentTime > enemyAircraftData.rateOfFire)
        {
            currentTime = 0f;

            var bullet = GameController.Instance.GetEnemyObjectPoolController.SpawnEnemyBulletFromPool(gameObject.GetComponentsInParent<IGetObjectPoolTag>()[0].GetObjectPoolTag());
            bullet.transform.position = gameObject.transform.position;
            bullet.GetComponent<Rigidbody2D>().AddForce(Vector3.down * enemyAircraftData.bulletSpeed, ForceMode2D.Impulse);
        }
    }
}