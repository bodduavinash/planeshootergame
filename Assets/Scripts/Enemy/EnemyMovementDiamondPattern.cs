﻿using System.Collections;
using UnityEngine;

public class EnemyMovementDiamondPattern : EnemyMovement, ITryRespawn
{
    const int noOfPlanes = 4;
    int planesDestroyed = 0;

    private EnemyAircraftScriptableObject enemyAircraftData;

    private float minScreenY;
    private float minScreenX, maxScreenX;

    private Vector3 playerPosition;

    protected override void OnEnable()
    {
        base.OnEnable();

        planesDestroyed = 0;

        var gameObjects = gameObject.GetComponentsInChildren<EnemyShoot>(true);

        foreach (var go in gameObjects)
        {
            go.transform.gameObject.SetActive(true);
        }

        enemyAircraftData = GetComponent<EnemyAI>().enemyAircraftData;

        minScreenY = cameraToWorld.y;

        maxScreenX = cameraToWorld.x;
        minScreenX = -maxScreenX;

        InitPosition();
    }

    private void InitPosition()
    {
        gameObject.transform.position = Vector3.up * (minScreenY + minScreenY / 2) + Vector3.right * Random.Range(minScreenX, maxScreenX);
        playerPosition = gameObject.transform.position;
    }

    public override void EnemyMove()
    {
        base.EnemyMove();

        playerPosition.y -= enemyAircraftData.movementSpeed * Time.deltaTime;
        gameObject.transform.position = playerPosition;
    }

    public void TryRespawn()
    {
        planesDestroyed++;

        if(planesDestroyed >= noOfPlanes)
        {
            GameController.Instance.GetEnemyObjectPoolController.ReturnEnemyAircraftToPool(gameObject);
            GameController.Instance.GetEnemyController.RespawnEnemyOfSameType(gameObject);
            GameController.Instance.GetEnemyController.CheckIfGameOver(gameObject.GetComponent<IGetObjectPoolTag>().GetObjectPoolTag());
        }
    }
}