using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMovementVerticle : EnemyMovement
{
    private EnemyAircraftScriptableObject enemyAircraftData;

    private float minScreenY;
    private float minScreenX, maxScreenX;

    private Vector3 playerPosition;

    protected override void OnEnable()
    {
        base.OnEnable();

        enemyAircraftData = GetComponent<EnemyAI>().enemyAircraftData;

        minScreenY = cameraToWorld.y;

        maxScreenX = cameraToWorld.x;
        minScreenX = -maxScreenX;

        InitPosition();
    }

    private void InitPosition()
    {
        gameObject.transform.position = Vector3.up * minScreenY + Vector3.right * Random.Range(minScreenX, maxScreenX);
        playerPosition = gameObject.transform.position;
    }

    public override void EnemyMove()
    {
        base.EnemyMove();

        playerPosition.y -= enemyAircraftData.movementSpeed * Time.deltaTime;
        gameObject.transform.position = playerPosition;
    }
}
