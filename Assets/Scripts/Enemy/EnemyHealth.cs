﻿using System.Collections;
using UnityEngine;


public class EnemyHealth : MonoBehaviour, IApplyDamage
{
    private EnemyAircraftScriptableObject enemyAircraftData;

    protected float health;

    private const string healthProperty = "_Health";
    //public float Health { get => health; }

    private void OnEnable()
    {
        enemyAircraftData = GetComponent<EnemyAI>().enemyAircraftData;

        InitHealth();
    }

    protected void InitHealth()
    {
        health = enemyAircraftData.maxHealth;
        ApplyHealthSlider(health);
    }

    protected void ApplyHealthSlider(float health)
    {
        gameObject.GetComponentInChildren<MeshRenderer>()?.material.SetFloat(healthProperty, health / enemyAircraftData.maxHealth);
    }

    public virtual void ApplyDamage(float damage)
    {
        health -= damage;

        ApplyHealthSlider(health);

        if (health <= 0)
        {
            GameController.Instance.GetEnemyObjectPoolController.ReturnEnemyAircraftToPool(gameObject);
            GameController.Instance.GetEnemyController.RespawnEnemyOfSameType(gameObject);
            GameController.Instance.GetEnemyController.CheckIfGameOver(gameObject.GetComponent<IGetObjectPoolTag>().GetObjectPoolTag());
        }
    }
}