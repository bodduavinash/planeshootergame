﻿using System.Collections;
using UnityEngine;

public class EnemyAI : MonoBehaviour, ITakeDamage
{
    public EnemyAircraftScriptableObject enemyAircraftData;

    private IShootable enemyShoot;
    private IEnemyMove enemyMovement;
    private IApplyDamage enemyhealth;

    protected virtual void OnEnable()
    {
        enemyMovement = GetComponent<IEnemyMove>();
        enemyShoot = GetComponent<IShootable>();
        enemyhealth = GetComponent<IApplyDamage>();
    }

    protected virtual void Update()
    {
        if (!GameController.Instance.IsGameStarted)
            return;

        enemyMovement?.EnemyMove();
        enemyShoot?.Shoot();
    }

    public virtual void TakeDamage(float damage)
    {
        enemyhealth?.ApplyDamage(damage);
        GameController.Instance.GetUIController.AddScore(enemyAircraftData.points);
    }
}