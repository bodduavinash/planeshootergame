using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class EnemyMovementHorizontalPingPong : EnemyMovement
{
    private EnemyAircraftScriptableObject enemyAircraftData;

    private float initPositionDuration = 0.5f;
    private float minScreenY;
    private float leftX, rightX;

    private bool isMovingLeft = true;

    private Vector2 spriteSize;

    protected override void OnEnable()
    {
        base.OnEnable();

        enemyAircraftData = GetComponent<EnemyAI>().enemyAircraftData;

        spriteSize = Utils.GetSpriteSize(transform);

        minScreenY = cameraToWorld.y;

        leftX = -(cameraToWorld.x + spriteSize.x) / 2;
        rightX = (cameraToWorld.x + spriteSize.x) / 2;

        InitPosition();
    }

    private void InitPosition()
    {
        gameObject.transform.position = Vector3.up * minScreenY;

        gameObject.transform.DOMoveY(enemyAircraftData.stopPosition.y, initPositionDuration).OnComplete(() => { isMovingLeft = false; EnemyMove(); }).SetAutoKill(true);
    }

    public override void EnemyMove()
    {
        base.EnemyMove();

        if(isMovingLeft)
        {
            MoveToRight();
        }
        else if (!isMovingLeft)
        {
            MoveToLeft();
        }
    }

    private void MoveToLeft()
    {
        gameObject.transform.DOMoveX(leftX, enemyAircraftData.movementSpeed).SetEase(enemyAircraftData.easeType).OnComplete(() => { isMovingLeft = true; });
    }

    private void MoveToRight()
    {
        gameObject.transform.DOMoveX(rightX, enemyAircraftData.movementSpeed).SetEase(enemyAircraftData.easeType).OnComplete(() => { isMovingLeft = false; });
    }
}