using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHealthType3 : EnemyHealth
{
    public override void ApplyDamage(float damage)
    {
        health -= damage;

        ApplyHealthSlider(health);

        if (health <= 0)
        {
            gameObject.SetActive(false);
            gameObject.GetComponentInParent<ITryRespawn>()?.TryRespawn();
        }
    }
}
