﻿using System.Collections;
using UnityEngine;

public interface IGetObjectPoolItem
{
    ObjectPoolItem GetObjectPoolItem();        
}