﻿using System.Collections;
using UnityEngine;

public interface ITryRespawn
{
    void TryRespawn();
}