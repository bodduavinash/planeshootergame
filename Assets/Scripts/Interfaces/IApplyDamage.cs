using UnityEngine;

public interface IApplyDamage
{
    abstract void ApplyDamage(float damage);
}
