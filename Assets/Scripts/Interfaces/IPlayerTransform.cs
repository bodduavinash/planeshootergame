using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IPlayerTransform
{
    Transform PlayerTransform { get; }
}
