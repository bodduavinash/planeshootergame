﻿using System.Collections;
using UnityEngine;


public interface IShootable
{
    void Shoot();
}