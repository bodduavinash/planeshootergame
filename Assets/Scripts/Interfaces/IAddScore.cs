using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IAddScore
{
    void AddScore(int score);
}
