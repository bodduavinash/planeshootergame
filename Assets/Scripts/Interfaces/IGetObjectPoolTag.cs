﻿using System.Collections;
using UnityEngine;

public interface IGetObjectPoolTag
{
    string GetObjectPoolTag();
}