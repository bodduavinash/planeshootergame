﻿using System.Collections;
using UnityEngine;

public interface ICollectables
{
    void Collect(GameObject gameObject);
}