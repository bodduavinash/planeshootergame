﻿using System.Collections;
using UnityEngine;

public interface IAddHealth
{
    void AddHealth(float health);
}