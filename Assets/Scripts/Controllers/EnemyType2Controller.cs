﻿using System.Collections;
using UnityEngine;

public class EnemyType2Controller : MonoBehaviour, ISpawnEnemy
{
    public void SpawnEnemy(string tag)
    {
        var gameObject = GameController.Instance.GetEnemyObjectPoolController.SpawnEnemyAircraftFromPool(tag);
    }
}