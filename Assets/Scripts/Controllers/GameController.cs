using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

[DefaultExecutionOrder(20)]
public class GameController : SingletonWithMonoBehavior<GameController>
{
    [Header("Controllers")]
    [SerializeField] private PlayerController playerController;
    [SerializeField] private LevelController levelController;
    [SerializeField] private UIController uiController;
    [SerializeField] private EnemyController enemyController;
    [SerializeField] private CollectablesController collectablesController;
    [SerializeField] private EnemyObjectPoolController enemyObjectPoolController;
    [SerializeField] private SaveController saveController;

    [Header("Config Data SO")]
    [SerializeField] private PlayerScriptableObject playerData;
    [SerializeField] private EnemyAircraftScriptableObject enemyType1Data;

    private bool isGameStarted = false;
    private bool isGameOver = false;

    public PlayerController GetPlayerController => playerController;
    public LevelController LevelController => levelController;
    public UIController GetUIController => uiController;
    public EnemyController GetEnemyController => enemyController;
    public CollectablesController CollectablesController => collectablesController;
    public EnemyObjectPoolController GetEnemyObjectPoolController => enemyObjectPoolController;

    public PlayerScriptableObject PlayerData => playerData;
    public EnemyAircraftScriptableObject EnemyType1Data => enemyType1Data;

    public bool IsGameStarted { get => isGameStarted; set => isGameStarted = value; }

    public bool IsGameOver { get => isGameOver; set => isGameOver = value; }

    [RuntimeInitializeOnLoadMethod]
    public SaveController GetSaveController()
    {
        return saveController;
    }
}
