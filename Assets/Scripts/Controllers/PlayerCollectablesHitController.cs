using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCollectablesHitController : MonoBehaviour
{
    private const string CollectableTag = "Collectables";

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag(CollectableTag))
        {
            collision.gameObject.GetComponent<ICollectables>().Collect(collision.gameObject);
        }
    }
}
