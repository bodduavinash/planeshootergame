using System.Collections.Generic;
using UnityEngine;

public class EnemyObjectPoolController : MonoBehaviour
{
    private const string aircraftPoolStr = "Aircraft";
    private const string bulletPoolStr = "Bullet";

    [SerializeField]
    private List<EnemySpawnAircraftPoolItem> enemySpawnAircraftPoolLists;

    private void Start()
    {
        InitEnemyObjectPool();
    }

    private void InitEnemyObjectPool()
    {
        if(enemySpawnAircraftPoolLists.Count > 0)
        {
            foreach(var enemyType in enemySpawnAircraftPoolLists)
            {
                enemyType.enemySpawnAircraftPool = new ObjectPoolItem(enemyType.enemySpawnAircraftPool.prefabGameObject.GetComponent<IGetObjectPoolTag>().GetObjectPoolTag()
                                                                        + aircraftPoolStr, 
                                                                        enemyType.enemySpawnAircraftPool.prefabGameObject,
                                                                        enemyType.enemySpawnAircraftPool.noOfObjectsToPool);

                enemyType.enemyBulletPool = new ObjectPoolItem(enemyType.enemyBulletPool.prefabGameObject.GetComponent<IGetObjectPoolTag>().GetObjectPoolTag()
                                                                + bulletPoolStr,
                                                                enemyType.enemyBulletPool.prefabGameObject,
                                                                enemyType.enemyBulletPool.noOfObjectsToPool);
            }
        }
    }

    public GameObject SpawnEnemyAircraftFromPool(string tag)
    {
        return enemySpawnAircraftPoolLists.Find(x => x.poolNameTag.Equals(tag)).GetEnemySpawnPooledObjectItem
            .enemySpawnAircraftPool.GetPooledObjectItem.GetObjectFromPool();
    }

    public void ReturnEnemyAircraftToPool(GameObject gameObject)
    {
        var tag = gameObject.GetComponent<IGetObjectPoolTag>().GetObjectPoolTag();

        enemySpawnAircraftPoolLists.Find(x => x.poolNameTag.Equals(tag)).GetEnemySpawnPooledObjectItem
            .enemySpawnAircraftPool.GetPooledObjectItem.ReturnObjectToPool(gameObject);
    }

    public GameObject SpawnEnemyBulletFromPool(string tag)
    {
        return enemySpawnAircraftPoolLists.Find(x => x.poolNameTag.Equals(tag)).GetEnemySpawnPooledObjectItem
            .enemyBulletPool.GetPooledObjectItem.GetObjectFromPool();
    }

    public void ReturnEnemyBulletToPool(GameObject gameObject)
    {
        var tag = gameObject.GetComponent<IGetObjectPoolTag>().GetObjectPoolTag();
        enemySpawnAircraftPoolLists.Find(x => x.poolNameTag.Equals(tag)).GetEnemySpawnPooledObjectItem
            .enemyBulletPool.GetPooledObjectItem.ReturnObjectToPool(gameObject);
    }

    public EnemyAircraftScriptableObject GetEnemyDataOfType(string tag)
    {
        var gameObject = SpawnEnemyAircraftFromPool(tag);

        var enemyData = gameObject.GetComponent<EnemyAI>().enemyAircraftData;

        ReturnEnemyAircraftToPool(gameObject);

        return enemyData;
    }
}