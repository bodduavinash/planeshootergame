using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerController : MonoBehaviour, ITakeDamage
{
    public PlayerScriptableObject playerData;

    private Rigidbody2D playerRigidbody;

    private PlayerInput playerInput;
    private PlayerMovement playerMovement;
    private PlayerShoot playerShoot;
    private PlayerShootCollectible playerShootCollectible;
    private IApplyDamage playerHealth;
    private IShootable shootable;

    public IShootable PlayerShoot => shootable;
    public GameObjectPool BulletPool => shootable.GetType() == playerShoot.GetType() ? playerShoot.GetPooledObject : playerShootCollectible.GetPooledObject;

    public PlayerScriptableObject PlayerData => playerData;

    private void Start()
    {
        playerRigidbody = GetComponent<Rigidbody2D>();

        playerInput = new PlayerInput();
        playerMovement = new PlayerMovement(playerInput.PlayerInputAction, playerData, playerRigidbody);
        playerShoot = new PlayerShoot(playerData, playerMovement.PlayerTransform);
        playerShootCollectible = new PlayerShootCollectible(playerData, playerMovement.PlayerTransform);
        playerHealth = GetComponent<IApplyDamage>();

        shootable = playerShoot;
    }

    private void Update()
    {
        if (!GameController.Instance.IsGameStarted || playerMovement == null)
            return;

        playerMovement?.Update();
        shootable?.Shoot();
    }

    public void TakeDamage(float damage)
    {
        playerHealth?.ApplyDamage(damage);
    }

    public void ChangeShootingType()
    {
        if(shootable == playerShoot)
        {
            shootable = playerShootCollectible;
        }
    }
}
