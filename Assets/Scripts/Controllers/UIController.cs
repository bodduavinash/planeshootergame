using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UIController : MonoBehaviour, IAddScore
{
    [SerializeField] private TextMeshPro scoreText;

    [SerializeField] private GameObject PlayGamePanel;
    [SerializeField] private GameObject GameOverPanel;

    private PlayerScore playerScore;

    public PlayerScore PlayerScore => playerScore;

    private void Start()
    {
        ShowPlayGamePanel(true);
        ShowGameOverPanel(false);
    }

    private void Awake()
    {
        playerScore = new PlayerScore();
    }

    public void AddScore(int score)
    {
        playerScore.AddScore(scoreText, score);
    }

    public void OnClickPlayGame()
    {
        GameController.Instance.IsGameStarted = true;
        GameController.Instance.IsGameOver = false;

        ShowPlayGamePanel(false);
        ShowGameOverPanel(false);
    }

    public void OnClickRestartGame()
    {
        SceneManager.LoadScene("GameScene");

        ShowPlayGamePanel(false);
        ShowGameOverPanel(false);
    }

    public void ShowPlayGamePanel(bool show)
    {
        PlayGamePanel.SetActive(show);
        PlayGamePanel.GetComponent<PlayGameUI>().highScore.text = GameController.Instance.GetSaveController().SaveData.highScore.ToString();
    }

    public void ShowGameOverPanel(bool show)
    {
        GameOverPanel.SetActive(show);

        if(PlayerScore.GetScore > playerScore.HighScore)
        {
            playerScore.HighScore = playerScore.GetScore;
            GameController.Instance.GetSaveController().SaveData.highScore = playerScore.GetScore;
            GameController.Instance.GetSaveController().SaveJsonData(GameController.Instance.GetSaveController().SaveData);
        }

        GameOverPanel.GetComponent<GameOverUI>().playerScore.text = playerScore.GetScore.ToString();
        GameOverPanel.GetComponent<GameOverUI>().highScore.text = playerScore.HighScore.ToString();
    }
}
