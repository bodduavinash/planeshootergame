using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyType3Controller : MonoBehaviour, ISpawnEnemy
{
    public void SpawnEnemy(string tag)
    {
        var gameObject = GameController.Instance.GetEnemyObjectPoolController.SpawnEnemyAircraftFromPool(tag);
    }
}
