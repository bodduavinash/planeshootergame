﻿using System.Collections;
using UnityEngine;

public class EnemyType1Controller : MonoBehaviour, ISpawnEnemy
{
    public void SpawnEnemy(string tag)
    {
        var gameObject = GameController.Instance.GetEnemyObjectPoolController.SpawnEnemyAircraftFromPool(tag);
    }
}