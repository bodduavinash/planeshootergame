﻿using System.Collections;
using System.IO;
using UnityEngine;

public class SaveController : MonoBehaviour
{
    private string filePath;
    private SaveData saveData;

    public SaveData SaveData { get => saveData; set => saveData = value; }

    private void Awake()
    {
        filePath = Application.dataPath + "/GameSaves/savedata.json";
        saveData = new SaveData();
        LoadJsonData();
    }

    [RuntimeInitializeOnLoadMethod]
    public void LoadJsonData()
    {
        if(!File.Exists(filePath))
        {
            Debug.Log($"File path does not exist at {filePath}");
            Debug.Log($"Creating the file now at {filePath}");

            SaveJsonData(saveData);
        }

        string loadData = File.ReadAllText(filePath);
        saveData = JsonUtility.FromJson<SaveData>(loadData);
        GameController.Instance.GetUIController.PlayerScore.HighScore = saveData.highScore;
    }

    public void SaveJsonData(SaveData data)
    {
        if(File.Exists(filePath))
        {
            string jsonSaveData = JsonUtility.ToJson(data, true);
            File.WriteAllText(filePath, jsonSaveData);
        }
    }
}