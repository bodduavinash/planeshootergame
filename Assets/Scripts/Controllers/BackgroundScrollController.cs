using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundScrollController : MonoBehaviour
{
    [SerializeField]
    private Vector2 scrollSpeed;

    private Vector2 textureOffset;
    private MeshRenderer meshRenderer;

    private void Start()
    {
        meshRenderer = GetComponent<MeshRenderer>();
        textureOffset = meshRenderer.material.mainTextureOffset;
    }

    private void Update()
    {
        textureOffset += scrollSpeed * Time.deltaTime;

        meshRenderer.material.mainTextureOffset = textureOffset;
    }
}
