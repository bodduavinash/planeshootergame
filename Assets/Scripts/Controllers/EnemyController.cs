﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour
{
    private EnemyType1Controller enemyType1Controller;
    private EnemyType2Controller enemyType2Controller;
    private EnemyType3Controller enemyType3Controller;

    private bool canType1Spawn, canType2Spawn, canType3Spawn;
    private int noOfPlanesDestroyedOfType1, noOfPlanesDestroyedOfType2, noOfPlanesDestroyedOfType3;

    private void OnEnable()
    {
        canType1Spawn = canType2Spawn = canType3Spawn = true;

        noOfPlanesDestroyedOfType1 = noOfPlanesDestroyedOfType2 = noOfPlanesDestroyedOfType3 = 0;
    }

    private void Start()
    {
        Type3Aircraft("Type1");
    }

    private void Type1Aircraft(string tag)
    {
        if (canType1Spawn)
        {
            enemyType1Controller = GetComponent<EnemyType1Controller>();
            enemyType1Controller.SpawnEnemy(tag);

            if(noOfPlanesDestroyedOfType1 > GameController.Instance.LevelController.LevelData.PlanesType1.noOfPlanesOfType)
            {
                canType1Spawn = false;
            }
        }
    }

    public void Type2Aircraft(string tag)
    {
        if (canType2Spawn)
        {
            enemyType2Controller = GetComponent<EnemyType2Controller>();
            enemyType2Controller.SpawnEnemy(tag);

            if(noOfPlanesDestroyedOfType2 > GameController.Instance.LevelController.LevelData.PlanesType2.noOfPlanesOfType)
            {
                canType2Spawn = false;
            }
        }
    }

    public void Type3Aircraft(string tag)
    {
        if (canType3Spawn)
        {
            enemyType3Controller = GetComponent<EnemyType3Controller>();
            enemyType3Controller.SpawnEnemy(tag);

            if(noOfPlanesDestroyedOfType3 > GameController.Instance.LevelController.LevelData.PlanesType3.noOfPlanesOfType)
            {
                canType3Spawn = false;
            }
        }
    }

    public void RespawnEnemyOfSameType(GameObject gameObject)
    {
        var type = gameObject.GetComponent<ITakeDamage>().GetType();
        var tag = gameObject.GetComponent<IGetObjectPoolTag>().GetObjectPoolTag();

        if(type.Equals(typeof(EnemyAircraftAIType1)))
        {
            Type1Aircraft(tag);
        }
        else if(type.Equals(typeof(EnemyAircraftAIType2)))
        {
            Type2Aircraft(tag);
        }
        else if (type.Equals(typeof(EnemyAircraftAIType3)))
        {
            Type3Aircraft(tag);
        }
    }

    public void CheckIfGameOver(string tag)
    {
        switch(tag)
        {
            case "Type1":
                if (noOfPlanesDestroyedOfType1 > GameController.Instance.LevelController.LevelData.PlanesType1.noOfPlanesOfType)
                {
                    canType1Spawn = false;
                }

                noOfPlanesDestroyedOfType1++;
                break;

            case "Type2":
                if (noOfPlanesDestroyedOfType2 > GameController.Instance.LevelController.LevelData.PlanesType2.noOfPlanesOfType)
                {
                    canType2Spawn = false;
                }

                noOfPlanesDestroyedOfType2++;
                break;

            case "Type3":
                if (noOfPlanesDestroyedOfType3 > GameController.Instance.LevelController.LevelData.PlanesType3.noOfPlanesOfType)
                {
                    canType3Spawn = false;
                }

                noOfPlanesDestroyedOfType3++;
                break;
        }

        if (!canType1Spawn && !canType2Spawn && !canType3Spawn)
        {
            GameController.Instance.IsGameOver = true;
            GameController.Instance.IsGameStarted = false;
            GameController.Instance.GetUIController.ShowGameOverPanel(true);
        }
    }
}