﻿using System.Collections;
using UnityEngine;

public class CollectablesController : MonoBehaviour
{
    [SerializeField] private CollectableScriptableObject collectableData;
    [SerializeField] private GameObject healthCollectablePrefab;
    [SerializeField] private GameObject shootCollectablePrefab;

    private bool isHealthCollectableSpawned;
    private bool isShootCollectableSpawned;

    public CollectableScriptableObject CollectableData => collectableData;

    private void OnEnable()
    {
        isHealthCollectableSpawned = isShootCollectableSpawned = true;
    }

    private void Update()
    {
        CheckHealthCollectable();
        CheckShootCollectable();
    }

    private void CheckHealthCollectable()
    {
        if(GameController.Instance.GetUIController.PlayerScore.GetScore % 5000 >= 0 &&
            GameController.Instance.GetUIController.PlayerScore.GetScore % 5000 <= 1000 &&
            !isHealthCollectableSpawned)
        {
            Instantiate(healthCollectablePrefab);
            isHealthCollectableSpawned = true;
        }
        else if(GameController.Instance.GetUIController.PlayerScore.GetScore % 5000 > 1000 && isHealthCollectableSpawned)
        {
            isHealthCollectableSpawned = false;
        }
    }

    private void CheckShootCollectable()
    {
        if (GameController.Instance.GetUIController.PlayerScore.GetScore % 10000 >= 0 &&
            GameController.Instance.GetUIController.PlayerScore.GetScore % 10000 <= 1000 &&
            !isShootCollectableSpawned)
        {
            Instantiate(shootCollectablePrefab, Vector3.up, Quaternion.identity);
            isShootCollectableSpawned = true;
        }
        else if (GameController.Instance.GetUIController.PlayerScore.GetScore % 10000 > 1000 && isShootCollectableSpawned)
        {
            isShootCollectableSpawned = false;
        }
    }
}