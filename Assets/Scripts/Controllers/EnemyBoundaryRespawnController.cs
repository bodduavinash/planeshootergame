﻿using System.Collections;
using UnityEngine;

public class EnemyBoundaryRespawnController : MonoBehaviour
{
    protected const string BoundaryTag = "Boundary";

    protected virtual void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.CompareTag(BoundaryTag))
        {
            GameController.Instance.GetEnemyObjectPoolController.ReturnEnemyAircraftToPool(gameObject);
            GameController.Instance.GetEnemyController.RespawnEnemyOfSameType(gameObject);
            GameController.Instance.GetEnemyController.CheckIfGameOver(gameObject.GetComponent<IGetObjectPoolTag>().GetObjectPoolTag());
        }
    }
}
