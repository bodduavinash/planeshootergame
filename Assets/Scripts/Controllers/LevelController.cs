﻿using System.Collections;
using UnityEngine;

public class LevelController : MonoBehaviour
{
    [SerializeField]
    private LevelScriptableObject levelData;

    public LevelScriptableObject LevelData => levelData;

    private bool canSpawningOfType1, canSpawningOfType2;

    private void OnEnable()
    {
        canSpawningOfType1 = canSpawningOfType2 = true;
    }

    private void Update()
    {
        CheckNoOfPlanes();
    }

    private void CheckNoOfPlanes()
    {
        if (GameController.Instance.GetUIController.PlayerScore.GetScore > levelData.PlanesType1.minPlayerScoreToSpawn
            && canSpawningOfType1)
        {
            canSpawningOfType1 = false;
            GameController.Instance.GetEnemyController.Type2Aircraft("Type2");
        }
        else if (GameController.Instance.GetUIController.PlayerScore.GetScore > levelData.PlanesType2.minPlayerScoreToSpawn
            && canSpawningOfType2)
        {
            canSpawningOfType2 = false;
            GameController.Instance.GetEnemyController.Type3Aircraft("Type3");
        }
    }
}