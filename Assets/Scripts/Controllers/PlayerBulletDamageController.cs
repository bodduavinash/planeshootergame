using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBulletDamageController : MonoBehaviour
{
    private const string BoundaryTag = "Boundary";
    private const string BulletTag = "Bullets";
    private const string EnemyTag = "Enemy";

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag(BoundaryTag) ||
            collision.CompareTag(BulletTag))
        {
            GameController.Instance.GetPlayerController.BulletPool.ReturnObjectToPool(gameObject);
        }
        else if(collision.CompareTag(EnemyTag))
        {
            GameController.Instance.GetPlayerController.BulletPool.ReturnObjectToPool(gameObject);
            collision.GetComponent<ITakeDamage>()?.TakeDamage(GameController.Instance.PlayerData.bulletDamage);
        }
    }
}