using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBulletDamageController : MonoBehaviour
{
    private const string BoundaryTag = "Boundary";
    private const string PlayerTag = "Player";

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag(BoundaryTag))
        {
            GameController.Instance.GetEnemyObjectPoolController.ReturnEnemyBulletToPool(gameObject);
        }
        else if(collision.CompareTag(PlayerTag))
        {
            GameController.Instance.GetEnemyObjectPoolController.ReturnEnemyBulletToPool(gameObject);
            collision.GetComponent<ITakeDamage>().TakeDamage(GameController.Instance.GetEnemyObjectPoolController.GetEnemyDataOfType(gameObject.GetComponent<IGetObjectPoolTag>().GetObjectPoolTag()).bulletDamage);
        }
    }
}
